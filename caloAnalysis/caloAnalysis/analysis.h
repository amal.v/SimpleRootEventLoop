#ifndef caloAnalysis_analysis_H
#define caloAnalysis_analysis_H

#include <EventLoop/Algorithm.h>
#include <TTree.h>
#include "xAODRootAccess/TStore.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetMatchingTools/DeltaRJetMatchingTool.h"

class analysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;


  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  xAOD::TStore* m_store; //!

  TTree* OutTree; //!
  std::string outputName;

  JetCalibrationTool* m_jetCalibration140; //!
  DeltaRJetMatchingTool* jetMatchingTool; //!

  double event_mu ; //!
  int event_ntruthjets; //!
  int event_nrecojets; //!

  //double event_NPV ; //!

  std::vector<double> jet_pt ; //!
  std::vector<double> jet_eta ; //!     
  std::vector<double> jet_phi ; //!     
  std::vector<double> jet_e ; //!     
  std::vector<double> jet_mass ; //!

  std::vector<double> truth_jet_pt ; //!
  std::vector<double> truth_jet_eta ; //!
  std::vector<double> truth_jet_phi ; //!
  std::vector<double> truth_jet_e ; //!
  std::vector<double> truth_jet_mass ; //!

  std::vector<double> ptreco_minus_true; //!
  std::vector<double> ptreco_over_true; //!
  
  // this is a standard constructor
  analysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(analysis, 1);
};

#endif
