#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/Sample.h"

#include "caloAnalysis/analysis.h"
#include <EventLoop/OutputStream.h>
#include <EventLoopAlgs/NTupleSvc.h> 

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:                                                                                         
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:                                                                                                               
  xAOD::Init().ignore();

  // Construct the samples to run on:                                                                                                              
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:                                             
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/a/avaidya/private/PFlow/StudyCalHits_2.1.27/samples");                 
  const char* inputFilePath = gSystem->ExpandPathName ("samples/user.avaidya") ;

  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/a/avaidya/private/CaloCells/athena/samples/mc15_14TeV.147915.Pythia8_AU2CT10_jetjet_JZ5W.recon.ESD.e1996_s2630_s2183_r7768") ;   

  SH::ScanDir().filePattern("user.avaidya.10024248.EXT0._000007.AOD.root").scan(sh, inputFilePath);
  //SH::ScanDir().filePattern("ESD.07978371._000988.pool.root.1").scan(sh, inputFilePath);

  // Set the name of the input TTree. It's always "CollectionTree"                                                                                 
  // for xAOD files.                                                                                                                               
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:                                                                                                                          
  sh.print();

  // Create an EventLoop job:                                                                                                                      
  EL::Job job;
  job.sampleHandler( sh );
  job.options()->setDouble (EL::Job::optMaxEvents, -1);

  //Add the output stream for the tree                                                                                                             

  EL::OutputStream output  ("myOutput");
  job.outputAdd (output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
  job.algsAdd (ntuple);


  // Add our analysis to the job:                                                                                                                  
  analysis* alg = new analysis();

  job.algsAdd( alg );
  alg->outputName = "myOutput"; // give the name of the output to our algorithm                                                                    
  // Run the job using the local/direct driver:                                                                                                    
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  return 0;
}
