#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>

#include <EventLoop/Worker.h>
#include <caloAnalysis/analysis.h>
#include <AsgTools/MessageCheck.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODJet/JetAuxContainer.h"
#include <fastjet/PseudoJet.hh>
#include <TFile.h>

#include <SampleHandler/MetaDataSample.h>
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include <TLorentzVector.h>

// this is needed to distribute the algorithm to the workers
ClassImp(analysis)



analysis :: analysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode analysis :: setupJob (EL::Job& job)
{
  ANA_CHECK_SET_TYPE (EL::StatusCode);
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD ();
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)                             
  ANA_CHECK(xAOD::Init());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (outputName);
  OutTree = new TTree ("OutTree", "OutTree");
  OutTree->SetDirectory(outputFile);
  OutTree->Branch("event_mu", &event_mu) ;
  OutTree->Branch("event_ntruthjets", &event_ntruthjets) ;
  OutTree->Branch("event_nrecojets", &event_nrecojets) ;

  OutTree->Branch("jet_pt", &jet_pt) ;
  OutTree->Branch("jet_eta", &jet_eta) ;
  OutTree->Branch("jet_phi", &jet_phi) ;
  OutTree->Branch("jet_e", &jet_e) ;
  OutTree->Branch("jet_mass", &jet_mass) ;
  OutTree->Branch("truth_jet_pt", &truth_jet_pt) ;
  OutTree->Branch("truth_jet_eta", &truth_jet_eta) ;
  OutTree->Branch("truth_jet_phi", &truth_jet_phi) ;
  OutTree->Branch("truth_jet_e", &truth_jet_e) ;
  OutTree->Branch("truth_jet_mass", &truth_jet_mass) ;
  OutTree->Branch("ptreco_minus_true", &ptreco_minus_true);
  OutTree->Branch("ptreco_over_true", &ptreco_over_true); 
  


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)                             
  
  xAOD::TEvent* event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  Info("initialize()", "Number of events = %lli", event->getEntries() );


  const std::string name = "analysis"; //string describing the current thread, for logging
  TString jetAlgo = "AntiKt4LCTopo";  //String describing your jet collection, for example AntiKt4EMTopo or AntiKt4LCTopo (see below)
  TString config = "HLLHC/JES_MC15_HLLHC_r7768_May2016_v2.config"; //Path to global config used to initialize the tool (see below)
  TString calibSeq = "JetArea_Residual_Origin_EtaJES" ; //String describing the calibration sequence to apply (see below)
  bool isData = false; //bool describing if the events are data or from simulation

  std::cout << "boop just before calibration tool" << jetAlgo.Data()<<std::endl;
  m_jetCalibration140 = new JetCalibrationTool(name);
  
  ANA_CHECK( m_jetCalibration140->setProperty("JetCollection",jetAlgo.Data()) );
  ANA_CHECK( m_jetCalibration140->setProperty("ConfigFile",config.Data()) );
  ANA_CHECK( m_jetCalibration140->setProperty("CalibSequence",calibSeq.Data()) );
  ANA_CHECK( m_jetCalibration140->setProperty("IsData",isData) );

  std::cout << "boop just before initialisation" <<std::endl; 
  ANA_CHECK(m_jetCalibration140->initializeTool(name));
  jetMatchingTool = new DeltaRJetMatchingTool();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)

  xAOD::TEvent* event = wk()->xaodEvent();


   const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));
  double lumi = wk()->metaData()->castDouble("lumi");


  //const xAOD::CaloClusterContainer* TopoClusters;
  const xAOD::JetContainer* antikt4jets; 
  //const xAOD::JetContainer* antikt10jets;
  const xAOD::JetContainer* AntiKt4TruthJets;
  //const xAOD::JetContainer* AntiKt10TruthJets;

  //ANA_CHECK( event->retrieve( AntiKt10TruthJets,"AntiKt10TruthJets" ));
  ANA_CHECK( event->retrieve( AntiKt4TruthJets, "AntiKt4TruthJets" ));
  //ANA_CHECK( event->retrieve( TopoClusters, "CaloCalTopoClusters" ));
  ANA_CHECK( event->retrieve( antikt4jets, "AntiKt4LCTopoJets" ));
  //ANA_CHECK( event->retrieve( antikt10jets, "AntiKt10LCTopoJets" ));

  //xAOD::TStore store;
  event_ntruthjets = AntiKt4TruthJets->size();
  event_nrecojets = antikt4jets->size();
  event_mu = eventInfo->averageInteractionsPerCrossing () ;
  std::cout << " number of pileup jets: " << event_nrecojets - event_ntruthjets << std::endl;
  std::cout << " number of truth jets: " << event_ntruthjets << std::endl;

  double dRantikt4 = 0.3;

  jetMatchingTool->DecorateMatchJets(*antikt4jets, *AntiKt4TruthJets, dRantikt4, "matchedTruth", "matchedReco" );

  
  for ( auto *ijet : *antikt4jets ) {

    if (ijet->pt()/1000 < 10 ) continue;

    xAOD::Jet* jet = 0;
    const xAOD::Jet* tjet = ijet->auxdecor< const xAOD::Jet* > ("matchedTruth");

    if (tjet == NULL) {
      std::cout << "null pointer, no matched truth, pile-up jet?" <<std::endl;
      continue;
    }

    xAOD::JetFourMom_t jetOriginP4;
    
    if ( ! ijet->getAttribute<xAOD::JetFourMom_t>("JetOriginConstitScaleMomentum",jetOriginP4) ){

    }

    m_jetCalibration140->calibratedCopy(*ijet,jet);

    double recopt = jet->pt();
    double truept = tjet->pt();

    jet_pt.push_back(     recopt  );
    jet_eta.push_back(    jet->eta() );
    jet_phi.push_back(    jet->phi() );
    jet_e.push_back(      jet->e()   );
    jet_mass.push_back(   jet->m()   );
    
    truth_jet_pt.push_back(   truept  );
    truth_jet_eta.push_back(  tjet->eta() );
    truth_jet_phi.push_back(  tjet->phi() );
    truth_jet_e.push_back(    tjet->e()   );
    truth_jet_mass.push_back( tjet->m()   );
    
    ptreco_minus_true.push_back( recopt - truept );
    ptreco_over_true.push_back( recopt / truept  );
    
    //    OutTree->Fill();


  }

  OutTree->Fill();

  jet_pt.clear();
  jet_eta.clear();
  jet_phi.clear();
  jet_e.clear();
  jet_mass.clear();

  truth_jet_pt.clear();
  truth_jet_eta.clear();
  truth_jet_phi.clear();
  truth_jet_e.clear();
  truth_jet_mass.clear();

  ptreco_minus_true.clear();
  ptreco_over_true.clear();

  /*
  std::cout << "total number of clusters: " << TopoClusters->size() << std::endl; 
  std::cout << "total number of akt 4 jets: " << antikt4jets->size() << std::endl;
  std::cout << "leading jet pt: " << antikt4jets->at(0)->pt()/1000 << std::endl;
  std::cout << "subleading jet pt: " << antikt4jets->at(1)->pt()/1000 << std::endl;
  std::cout << "************************************************* " << AntiKt4TruthJets->size() << std::endl;
  */

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode analysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}



