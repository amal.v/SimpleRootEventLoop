
Very simple event loop algorithm for AOD studies

Uses matching framework written by Arthur Bolz: https://gitlab.cern.ch/JSSTools/JetMatchingTools

Current version uses calibration tag used for HI-LUMI upgrade samples, users should find the appropriate JetCalibTools tag/config to use. 

local and grid runnign scripts can be found in caloAnalysis/util/